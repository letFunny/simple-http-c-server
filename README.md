Simple multi-threaded concurrent server written in C.

# Disclaimer
Not all the code in the repository is mine. I had nothing to do with the
multithreading nor the Options HTTP method. The rest of the code is somewhat
interleaved but the Git history was maintained so you can check who did what.

I especially worked on sockets, pipes for the processes, sending data and
receiving it efficiently. Also I designed the request validantion and its flow
through the program. The wiki was mostly written by me so I
encourage you to read it to find more about the design choices.

# Prerequisites
You must run some Linux flavored distro. The server has been tested in Arch
Linux and Ubuntu.

We use several libraries:
- libconfuse
- picohttpparser

You must have installed libconfuse in you sistem.
```sh
apt install libconfuse-dev
```
We include a copy of picohttpparser in our source files so there is not need
to download it.

# Compilation
In order to compile the program:
```sh
make
```
We also have some targets for recompiling (make re), cleaning (make fclean and
make clean)

# Execution
To execute the server run:
```sh
./server
```

# Config
With server.conf we can specify different parameters for the server such as:
- *server_root*: specify the folder where the files are stored.
- *max_clients*: maximum number of clients the server can attend at once.
- *listen_port*: port where the server will be listening.
- *server_signature*: The server signature indicated in http headers.
- *daemon_mode*: true/false. true if the server shoould be excuted in daemon mode.
- *number_clients*: indicates the number of clients the server will start attending at once.
The example format is under *server.conf* in the repository root.

# Documentation
Available in the wiki and in the header files. The source code in itself is very
redable and it is heavily commented.
