#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>

#include <response.h>
#include <pool_thread.h>
#include <log.h>
#include <socket.h>

struct pool_th {
    int num_th;
    int num_wk;
    pthread_mutex_t mutex;
    pthread_t *threads;
    int sockfd;
    config *conf;
};

/**
 * Number of threads currently working
 *
 * @param pool_thread pool of threads from which you want to know the number of
                      currently working threads
 *
 * @return number of working threads
 */
int pool_th_num_wk(pool_th *pool_thread);

/*
 * Function each thread will run
 *
 * @param args arg structure with the pool_th structure the thread belong to and
 *             config structure of the server.
 */
void *thread_do(pool_th *pool_thread);


pool_th *pool_th_ini(config *conf, int sockfd) {
    pool_th *pool_thread;
    int i, aux;
    int num_th = conf->num_clients;

    if (num_th < MIN_THREAD || num_th > conf->max_clients) {
        perror("num_th no válido");
        return NULL;
    }

    pool_thread = (pool_th *) malloc(sizeof(pool_th));
    if (pool_thread == NULL) {
        perror("Error reservando memoria");
        return NULL;
    }

    pool_thread->num_th = 0;
    pool_thread->num_wk = 0;
    pool_thread->sockfd = sockfd;

    if (pthread_mutex_init(&(pool_thread->mutex), NULL) != 0) {
        perror("mutex init failed");
        free(pool_thread);
        return NULL;
    }

    pool_thread->threads = (pthread_t *) malloc(num_th * sizeof(pthread_t));
    if (pool_thread->threads == NULL) {
        perror("Error reservando memoria");
        pthread_mutex_destroy(&(pool_thread->mutex));
        free(pool_thread);
        return NULL;
    }

    pool_thread->conf = conf;

    for (i = 0; i < num_th; i++) {
        if (pthread_create(&pool_thread->threads[i], NULL, (void *) thread_do,
                           pool_thread) != 0) {
            perror("Error creating threads");
            free(pool_thread->threads);
            pthread_mutex_destroy(&(pool_thread->mutex));
            free(pool_thread);
            return NULL;
        }
    }

    aux = 0;

    //we wait for all the threads to start working
    while (aux < num_th) {
        pthread_mutex_lock(&pool_thread->mutex);
        aux = pool_thread->num_th;
        pthread_mutex_unlock(&pool_thread->mutex);
    }

    return pool_thread;
}


void pool_th_destroy(pool_th *pool_thread) {
    int aux, num_th;
    if (pool_thread == NULL) return;
    num_th = pool_thread->num_th;

    for (int i = 0; i < num_th; i++)
        if (pool_thread->threads[i]) pthread_cancel(pool_thread->threads[i]);

    //we wait for the threads to end correctly
    aux = 1;
    while (aux > 0) {
        pthread_mutex_lock(&pool_thread->mutex);
        aux = pool_thread->num_th;
        pthread_mutex_unlock(&pool_thread->mutex);
    }

    free(pool_thread->threads);
    pthread_mutex_destroy(&(pool_thread->mutex));
    free(pool_thread);
}

int pool_th_num_wk(pool_th *pool_thread) {
    int aux;
    if (pool_thread != NULL) {
        pthread_mutex_lock(&pool_thread->mutex);
        aux = pool_thread->num_wk;
        pthread_mutex_unlock(&pool_thread->mutex);
        return aux;
    }

    return -1;
}

//reduce number of working threads
void pool_thread_reducenumth(void *arg) {
    pool_th *pool_thread = (pool_th *) arg;
    //wait for mutex
    pthread_mutex_lock(&pool_thread->mutex);
    pool_thread->num_th -= 1;
    pthread_mutex_unlock(&pool_thread->mutex);

}


void *thread_do(pool_th *pool_thread) {
    int clientfd;
    struct sockaddr_in client_addr;
    socklen_t addrlen = sizeof(client_addr);

    //We mask all signals for the threads
    sigset_t mask;
    sigfillset(&mask);
    if (pthread_sigmask(SIG_SETMASK, &mask, NULL) != 0) {
        log_message(LOG_ERR, "Error masking de signal: %m\n");
    }

    //to avoid having to join
    pthread_detach(pthread_self());
    //the thread can be cancelled asynchronously
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    //when the thread ends pool_thread_reducenumth is executed
    pthread_cleanup_push((void *) pool_thread_reducenumth, pool_thread)

            //the thread has been correctly created so we increase the pool's total number
            pthread_mutex_lock(&pool_thread->mutex);
            pool_thread->num_th += 1;
            pthread_mutex_unlock(&pool_thread->mutex);

            while (1) {
                clientfd = accept(pool_thread->sockfd, (struct sockaddr *) &client_addr, &addrlen);
                if (clientfd < 0)
                    break;

                //We set the thread as not cancelable so that the mutex isn't locked
                pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
                pthread_mutex_lock(&pool_thread->mutex);
                pool_thread->num_wk += 1;
                pthread_mutex_unlock(&pool_thread->mutex);
                pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

                connection(clientfd, client_addr, pool_thread->conf);

                pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
                pthread_mutex_lock(&pool_thread->mutex);
                pool_thread->num_wk -= 1;
                pthread_mutex_unlock(&pool_thread->mutex);
                pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
            }

    pthread_cleanup_pop(1);
    return 0;
}
