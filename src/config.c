#include <config.h>
#include <log.h>
#include <stdlib.h>
#include <confuse.h>
#include <unistd.h>
#include <string.h>
#include <linux/limits.h>

int daemon_mode;

config *get_server_config() {
    int ret;
    config *conf;
    cfg_t *cfg;
    cfg_bool_t daemon_mode_bool;
    char *server_root = NULL;
    char path[PATH_MAX];

    conf = malloc(sizeof(config));
    memset(conf, 0, sizeof(config));

    cfg_opt_t opts[] =
            {
                    CFG_SIMPLE_STR("server_root", &(server_root)),
                    CFG_SIMPLE_STR("server_signature", &(conf->server_signature)),
                    CFG_SIMPLE_INT("max_clients", &(conf->max_clients)),
                    CFG_SIMPLE_INT("listen_port", &(conf->listen_port)),
                    CFG_SIMPLE_BOOL("daemon_mode", &daemon_mode_bool),
                    CFG_SIMPLE_INT("number_clients", &(conf->num_clients)),
                    CFG_END()
            };
    cfg = cfg_init(opts, CFGF_NONE);
    if ((ret = cfg_parse(cfg, "server.conf")) != CFG_SUCCESS) {
        if (ret == CFG_FILE_ERROR)
            log_message(LOG_ERR, "Config file could not be opened");
        else if (ret == CFG_PARSE_ERROR)
            log_message(LOG_ERR, "Config file parsing error");
        exit(EXIT_FAILURE);
    }
    getcwd(path, sizeof(path));
    strcat(path, server_root);
    strcpy(conf->server_root, path);

    daemon_mode = daemon_mode_bool == cfg_true ? 1 : 0;
    cfg_free(cfg);
    free(server_root);
    return conf;
}


void destroy_server_config(config *conf) {
    free(conf->server_signature);
    free(conf);
}
