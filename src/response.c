#define _GNU_SOURCE

#include <unistd.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#include <log.h>
#include <response.h>
#include <response_header.h>
#include <request_parse.h>

#define READ 0
#define WRITE 1

static int execute_process(const char *command, const char *arg, const char *stdin) {
    pid_t pid;
    int child_in[2];
    int child_out[2];

    if (0 != pipe(child_in))
        return -1;
    if (0 != pipe(child_out)) {
        log_message(LOG_ERR, "Error in pipe: %m");
        close(child_in[0]);
        close(child_in[1]);
        return -1;
    }

    pid = fork();
    if (pid == -1) {
        log_message(LOG_ERR, "Error in fork: %m");
        return -1;
    } else if (pid != 0) {
        // Father
        // Close the child_in pipe for reading
        close(child_in[READ]);
        // Close the child_out pipe for writing
        close(child_out[WRITE]);
        // Write to the stdin of the process if we have something to send
        if (stdin)
            write(child_in[WRITE], stdin, strlen(stdin));
        // Close the stdin so the child does not lock
        close(child_in[WRITE]);

        // Wait for the child to die
        wait(NULL);

        return child_out[READ];
    }

    // Child
    // Close the child_out pipe for reading
    close(child_out[READ]);
    // Close the child_in pipe for writing
    close(child_in[WRITE]);
    // child_in to stdin
    dup2(child_in[READ], STDIN_FILENO);
    // child_out to stdout
    dup2(child_out[WRITE], STDOUT_FILENO);

    return execlp(command, command, arg, (char *) NULL);
}

static char *get_content_type(const char *ext) {
    // According to:
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
    if (strncmp(ext, "jpg", 4) == 0
        || strncmp(ext, "jpeg", 5) == 0
        || strncmp(ext, "jfif", 5) == 0
        || strncmp(ext, "pjeg", 5) == 0
        || strncmp(ext, "pjp", 4) == 0)
        return "image/jpeg";
    if (strncmp(ext, "png", 4) == 0)
        return "image/png";
    if (strncmp(ext, "html", 5) == 0
        || strncmp(ext, "htm", 4) == 0) {
        return "text/html";
    }
    if (strncmp(ext, "gif", 4) == 0)
        return "image/gif";
    if (strncmp(ext, "mpeg", 5) == 0
        || strncmp(ext, "mpg", 4) == 0)
        return "video/mpeg";
    if (strncmp(ext, "doc", 4) == 0
        || strncmp(ext, "docx", 5) == 0)
        return "application/msword";
    if (strncmp(ext, "pdf", 4) == 0)
        return "application/pdf";

    return "text/plain";
}

static RESPONSE_STATUS process_post(request *req, int connection_fd,
                                    char *server_signature) {
    int fd = -1;
    int size_int;
    char *header;
    char *content_type;
    // Preallocated buffer that will hold the header
    char buf[200];
    size_t send_size;

    content_type = get_content_type(req->extension);
    if (req->script == NONE) {
        // A request cannot get to here wihout an script
        return OTHER_ERROR;
    }

    if (req->script == PYTHON) {
        fd = execute_process("python", req->path, req->body);
    } else if (req->script == PHP) {
        fd = execute_process("php", req->path, req->body);
    }

    if (-1 == fd) {
        log_message(LOG_ERR, "Error executing process");
        return SEND_ERROR;
    }

    int ret = ioctl(fd, FIONREAD, &(size_int));
    if (-1 == ret) {
        log_message(LOG_ERR, "Error getting the size of the pipe: %m");
        return OTHER_ERROR;
    }
    send_size = size_int;

    header = header_craft(buf, req->minor_version, S_200, server_signature,
                          NULL, content_type, send_size, 0, req->script);
    if (-1 == send(connection_fd, header, strlen(header), 0)) {
        close(fd);
        return SEND_ERROR;
    }
    if (req->script == NONE) {
        if (-1 == sendfile(connection_fd, fd, NULL, send_size)) {
            close(fd);
            return SEND_ERROR;
        }
    } else {
        if (-1 == splice(fd, NULL, connection_fd, NULL, send_size, 0)) {
            close(fd);
            return SEND_ERROR;
        }
    }
    close(fd);
    return SEND_OK;
}

static RESPONSE_STATUS process_options(request *req, int connection_fd,
                                       char *server_signature) {
    char *header;
    // Preallocated buffer that will hold the header
    char buf[200];
    //content_type = get_content_type(req->extension);
    int send_size = 0;
    header = header_craft(buf, req->minor_version, S_200, server_signature,
                          &(req->last_modified), NULL, send_size, 1, req->script);

    if (-1 == send(connection_fd, header, strlen(header), 0))
        return SEND_ERROR;
    return SEND_OK;
}


static RESPONSE_STATUS process_get(request *req, int connection_fd, char *server_signature) {
    int fd = -1;
    char *header;
    char *content_type;
    // Preallocated buffer that will hold the header
    char buf[200];
    size_t send_size;

    content_type = get_content_type(req->extension);
    if (req->script == NONE) {
        fd = req->fd;
        send_size = req->file_size;
    } else {
        int size_int;
        if (req->script == PYTHON) {
            fd = execute_process("python", req->path, req->parameters);
        } else if (req->script == PHP) {
            fd = execute_process("php", req->path, req->parameters);
        }
        if (-1 == fd) {
            log_message(LOG_ERR, "Error executing process");
            return OTHER_ERROR;
        }
        int ret = ioctl(fd, FIONREAD, &(size_int));
        if (-1 == ret) {
            log_message(LOG_ERR, "Error getting the size of the pipe: %m");
            return OTHER_ERROR;
        }
        send_size = size_int;
    }
    header = header_craft(buf, req->minor_version, S_200, server_signature,
                          &(req->last_modified), content_type, send_size, 0, req->script);
    if (-1 == send(connection_fd, header, strlen(header), 0)) {
        close(fd);
        return SEND_ERROR;
    }
    if (req->script == NONE) {
        if (-1 == sendfile(connection_fd, fd, NULL, send_size)) {
            close(fd);
            return SEND_ERROR;
        }
    } else {
        if (-1 == splice(fd, NULL, connection_fd, NULL, send_size, 0)) {
            close(fd);
            return SEND_ERROR;
        }
    }
    close(fd);
    return SEND_OK;
}

RESPONSE_STATUS process_response(char *buf, ssize_t size, int connection_fd,
                                 config *conf, int *close) {
    request_error ret;
    request req;
    STATUS_CODE status;
    char *header;
    // Preallocated buffer that will hold the header
    char header_buf[200];

    ret = request_parse(buf, size, &req, conf->server_root);
    *close = req.close;

    switch (ret) {
        case INCOMPLETE_REQUEST:
            return RECV_INCOMPLETE;
        case INVALID_VERSION:
        case METHOD_NOT_ALLOWED:
            status = S_405;
            break;
        case PARSER_ERROR:
            status = S_400;
            break;
        case INVALID_FILE:
            status = S_404;
            break;
        case NOT_MODIFIED:
            status = S_304;
            break;
        case OK:
            status = S_200;
            break;
        default:
            status = S_500;
            break;
    }
    if (status == S_200 && strncmp(req.method, "GET", 4) == 0) {
        return process_get(&req, connection_fd, conf->server_signature);
    } else if (status == S_200 && strncmp(req.method, "POST", 5) == 0) {
        return process_post(&req, connection_fd, conf->server_signature);
    } else if (status == S_200 && strncmp(req.method, "OPTIONS", 8) == 0) {
        return process_options(&req, connection_fd, conf->server_signature);
    } else {
        header = header_craft(header_buf, req.minor_version, status,
                              conf->server_signature, NULL, "text/plain", 0, 0, req.script);
        if (NULL == header)
            return OTHER_ERROR;
        if (-1 == send(connection_fd, header, strlen(header), 0)) {
            return SEND_ERROR;
        }
    }
    return SEND_OK;
}
