#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <resolv.h>
#include <unistd.h>
#include <config.h>

#include <daemonize.h>
#include <log.h>
#include <pool_thread.h>
#include <socket.h>

pool_th *pool;
int sockfd;
config *conf;

void handler(int sig) {
    log_message(LOG_INFO, "Program stopping because of signal action %d\n", sig);
    pool_th_destroy(pool);
    close(sockfd);
    destroy_server_config(conf);
    exit(EXIT_SUCCESS);
}

int main(void) {
    struct sigaction act;

    // We mask all signals except for sigint and sigterm
    sigset_t mask;
    sigfillset(&mask);
    sigdelset(&mask, SIGINT);
    sigdelset(&mask, SIGTERM);
    if (sigprocmask(SIG_SETMASK, &mask, NULL) != 0) {
        log_message(LOG_ERR, "Error masking de signal: %m\n");
        exit(EXIT_FAILURE);
    }

    act.sa_handler = handler;
    sigemptyset(&(act.sa_mask));
    act.sa_flags = 0;
    if (sigaction(SIGINT, &act, NULL) < 0) {
        perror("sigaction sigint");
        exit(EXIT_FAILURE);
    }
    if (sigaction(SIGTERM, &act, NULL) < 0) {
        perror("sigaction sigterm");
        exit(EXIT_FAILURE);
    }

    conf = get_server_config();

    if (daemon_mode) {
        fprintf(stderr, "Daemon mode is enabled, forking...\n");
        daemonize();
    }

    sockfd = socket_ini(conf->listen_port);
    if (sockfd < 0) exit(errno);

    pool = pool_th_ini(conf, sockfd);
    if (!pool) exit(errno);

    pause();

    return 0;
}
