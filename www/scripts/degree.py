import sys
import signal
TIMEOUT = 1 # seconds
signal.signal(signal.SIGALRM, input)
signal.alarm(TIMEOUT)

for line in sys.stdin:
	d = dict(item.split("=") for item in line.split("&"))
	if('celsius' in d.keys()):
		num = float(d['celsius']) * 9 / 5 + 32
		print(num)
	else:
		print("Please povide the field celsius as an argument")

