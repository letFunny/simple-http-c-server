NAME = server
SRC_DIR = src
SRC_LIB_DIR = srclib
OBJ_DIR = obj
CFLAGS = -g -Iincludes -Wall -Wextra
CLIBS =  -lrt -lpthread -pthread -lconfuse

SRC = $(wildcard $(SRC_DIR)/*.c)
SRC_LIB = $(wildcard $(SRC_LIB_DIR)/*.c)
OBJ = $(subst $(SRC_DIR), $(OBJ_DIR), $(subst .c,.o,$(SRC)))
OBJ_LIB = $(subst $(SRC_LIB_DIR), $(OBJ_DIR), $(subst .c,.o,$(SRC_LIB)))

all: $(NAME)

$(NAME) : $(OBJ) $(OBJ_LIB)
	$(CC) $(CFLAGS) $(OBJ) $(OBJ_LIB) -o $(NAME) $(CLIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ $(CLIBS)

$(OBJ_DIR)/%.o: $(SRC_LIB_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@ $(CLIBS)

fclean: clean
	rm -f $(NAME)

clean:
	rm -f $(OBJ) $(OBJ_LIB)

re: fclean all

.PHONY: clean fclean
