#ifndef SOCKET_INI_H
#define SOCKET_INI_H

// Timeout in recv
#define TIME_OUT_SEC 30

/*
* Initializes socket listening in the port specified in server.conf
* @return file descriptor
*/
int socket_ini();

/*
 * Function that will run every time a connection is stabliched.
 * @param clientfd client connection file descriptor
 * @param client_addr
 * @param conf server configuration
 */
void connection(int clientfd, struct sockaddr_in client_addr, config *conf);


#endif
