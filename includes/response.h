#ifndef RESPONSE_H
#define RESPONSE_H

#include <config.h>

/**
 * Response errors. Basically self explanatory. OTHER_ERROR entails system errors or unexpected failures not related
 * to sending.
 */
typedef enum {
    SEND_ERROR,
    RECV_INCOMPLETE,
    SEND_OK,
    OTHER_ERROR,
} RESPONSE_STATUS;

/**
 * Given a request raw buffer. It creates the request object, perform the necessary checks and fullfills the request
 * sending a response. The error created are irrecoverable as we have no way of communication to the client if
 * we encounter a SEND_ERROR.
 * @param buf Buffer that contains the raw request.
 * @param size Size of the request buffer.
 * @param connection_fd Connection file descriptor from an accept call.
 * @param conf Configuration struct.
 * @param close int pointer to indicate if the connection should close after responding
 * @return Status if everything went right SEND_OK or if there was an error.
 */
RESPONSE_STATUS process_response(char *buf, ssize_t size, int connection_fd,
                                 config *conf, int *close);

#endif //RESPONSE_H
