#ifndef UTILS_H
#define UTILS_H

#include <time.h>
#include <string.h>

/**
 * From a time object, produces a string following the HTML date format. See:
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Date
 * The format was taken from:
 * https://stackoverflow.com/questions/7548759/generate-a-date-string-in-http-response-date-format-in-c/7548846
 * @param timep Time object.
 * @param buf Preallocated buffer that will hold the string.
 * @param length Total allocated size of the buffer.
 * @return 0 if OK, else -1.
 */
int time_to_string(const time_t *timep, char *buf, size_t length);

/**
 * Return the file extension by trimming the path of the file. It searchs for
 * the last dot and returns a pointer to the extension in the same string.
 * Be aware of changing it.
 * @param path Path to the file at least containing the whole name.
 * @return Pointer to the extension.
 */
char *get_file_extension(const char *path);

#endif //UTILS_H
