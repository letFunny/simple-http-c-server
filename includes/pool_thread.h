#ifndef POOL_THREAD_H
#define POOL_THREAD_H


/**
 * thread_pool.h
 *
 * Implements functionality relative to the pool of threads
 *
 */

#include <config.h>

//min number of threads for our pool
#define MIN_THREAD 1

//Pool thread structure
typedef struct pool_th pool_th;

/**
 * Initializes the pool of threads
 *
 * @param conf config structure returned by get_server_config
 * @param sockfd file descriptor of the socket the server uses
 *
 * @return pool_th structure
 */
pool_th *pool_th_ini(config *conf, int sockfd);

/*
 * Destroys the structure freeing its resources
 *
 *.@param pool_thread pool_th to free
 */
void pool_th_destroy(pool_th *pool_thread);


#endif
