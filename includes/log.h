#ifndef LOG_H
#define LOG_H

#include <stdarg.h>
#include <syslog.h>
#include <stdio.h>

#include <config.h>

/**
 * Simple interface to log messages for the program. If the extern variable daemon_mode found in config (in our case)
 * is set to 1, we will log to syslog, if it is set to zero, we log to stderr. When trying to log a errno error, please
 * add the modifier %m that will be substituted for the "strderr" string.
 * @param priority Priority according to the syslog API, man syslog for more information and possible values.
 * @param format Printf like format.
 * @param ... Arguments for the format.
 */
void log_message(int priority, const char *format, ...);

#endif //LOG_H
