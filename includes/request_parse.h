#ifndef PARSE_REQUEST_H
#define PARSE_REQUEST_H

#include <linux/limits.h>

#include <picohttpparser.h>

/**
 * Default number of headers that we support in an incoming request.
 */
#define NUM_HEADERS 30

/**
 * When parsing a request, all the possible errors that can appear and OK if the request has none. The different types
 * are self explanatory as the name is very descriptive.
 */
typedef enum {
    OK,
    INCOMPLETE_REQUEST,
    PARSER_ERROR,
    METHOD_NOT_ALLOWED,
    INVALID_FILE,
    INVALID_VERSION,
    NOT_MODIFIED,
} request_error;

/**
 * We only support Python or PHP scripts. We set the script type if the request is going to call a script and NONE if
 * it does not.
 */
typedef enum {
    PYTHON,
    PHP,
    NONE
} SCRIPT_TYPE;


/**
 * Request struct that is validated. It is very important to know that is has already been validated so no error (except
 * system ones) can arise. Files are already opened and times loaded. The whole request is loaded into a buffer and
 * the different members of the struct are pointer aliases to different positions (if possible).
 *  - method: request method "GET"/"POST"/"OPTIONS".
 *  - path: full absolute path to the request file.
 *  - body: pointer to the body of the request (or NULL if there is no body).
 *  - parameters: pointer to the parameters string of the request (or NULL if there are no parameters).
 *  - extension: file extension in order to, later on. determine the MIME type.
 *  - script: PYTHON/PHP/NONE self explanatory.
 *  - num_headers: the number of headers.
 *  - minor_version: the http minor version. We only support 1.1 but for the
 *  future or for checks.
 *  - fd: if the request has asked for a file this is the descriptor of that
 *  file opened, -1 else.
 *  - file_size: size of the requested file (if there is one).
 *  - last_modified: last modified size of the requested file.
 *  - headers: pointers to a struct from picohttpparser that contains the
 *  headers of the request. For more information look picohttpparser.
 */
typedef struct {
    const char *method;
    char path[PATH_MAX];
    const char *body;
    char *parameters;
    char *extension;
    SCRIPT_TYPE script;
    size_t num_headers;
    int minor_version;
    int fd;
    size_t file_size;
    time_t last_modified;
    struct phr_header headers[NUM_HEADERS];
    int close;
} request;

/**
 * Print the request for debugging purposes.
 * @param req Request that we will print.
 */
void print_request(request *req);

/**
 * This methods receives a pre-allocated request object and the buffer
 * containing the request. It parses the request and fills the object
 * appropriately.
 * @param buf Buffer containing the raw request.
 * @param buflen Length of the buffer.
 * @param req Pre-allocated request object that is going to be filled.
 * @return Status code indication if the parsing went OK or if error was encountered.
 */
request_error request_parse(char *buf, int buflen, request *req, char *path);

#endif //PARSE_REQUEST_H
