#ifndef CONFIG_H
#define CONFIG_H

#include <linux/limits.h>

/**
 * Extern variable that is to be set so that the logging function know if it should log to stdout (=0) or log to
 * syslog (=1). In our case it is set through the config.
 */
extern int daemon_mode;

/**
 * Defines the Config structure that we get from the file. We will use this configuration across the program.
 *  - server_root: route to the directory that contains the files/resources for the web server. It is a relative root
 *  from the directory where the config file is.
 *  - max_clients: maximum number of clients that the server can attend at the same time. If we read this limit, the
 *  server will not accept new connections.
 *  - listen_port: port where we listen to incoming connections.
 *  - server_signature: string that would be returned in each ServerName header.
 *  - num_clients: number of clients the server will start attending at once.
 */
typedef struct {
    char server_root[PATH_MAX];
    // This have to be long ints for libconfuse to work properly
    long int max_clients;
    long int listen_port;
    char *server_signature;
    long int num_clients;
} config;

/**
 * This function loads and parses the configuration file "server.conf" according to the format given in the example
 * provided with the code.
 * @return Configuration struct or NULL in case of error.
 */
config *get_server_config();

/**
 * Frees the server configuration struct to prevent leaks if needed.
 * @param conf Configuration struct.
 */
void destroy_server_config(config *conf);

#endif //CONFIG_H
