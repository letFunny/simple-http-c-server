#ifndef DAEMONIZE_H
#define DAEMONIZE_H

/**
 * A reimplementation of daemon() (man daemon on the Linux Manual). We redirect stdin, stdout and stderr to /dev/null.
 * We fork the process two types making it the leader of a group so that it does not depend on the terminal where it
 * was open. Lastly, we change the location so that if the folder where it was spawned was deleted, no errors happen.
 */
void daemonize(void);

#endif //DAEMONIZE_H
