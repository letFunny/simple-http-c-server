#ifndef RESPONSE_HEADER_H
#define RESPONSE_HEADER_H

#include <time.h>
#include <request_parse.h>

/** Inspired by:
 * https://stackoverflow.com/questions/9907160/how-to-convert-enum-names-to-string-in-c
 * Proper explanation and motivation in the project wiki pages. Basically, for each request we create one enum member,
 * and two arrays associating the enum code with its message and with its number.
*/
#define GENERATE_ENUM(ENUM, NUM, STR) ENUM,
#define GENERATE_STRING(ENUM, NUM, STR) STR,
#define GENERATE_NUM(ENUM, NUM, STR) NUM,
#define FOR_EACH_CODE(CODE) \
        CODE(S_200, 200, "OK") \
        CODE(S_304, 304, "Not Modified") \
        CODE(S_400, 400, "Bad Request") \
        CODE(S_404, 404, "Not Found") \
        CODE(S_405, 405, "Method Not Allowed") \
        CODE(S_500, 500, "Internal Server Error") \

extern const char *STATUS_STRINGS[];
extern const int STATUS_NUM[];

typedef enum {
    FOR_EACH_CODE(GENERATE_ENUM)
} STATUS_CODE;

/**
 * Creates the header for the server response following the HTML 1.0/1.1
 * standard. See:
 * https://tools.ietf.org/html/rfc7230
 * @param minor_version Minor version of the HTML request to respond in the
 * same one.
 * @param status_code Status code for the response. Check enum for available
 * ones.
 * @param server_signature String we are going to return in the "Server" field.
 * @param last_modified Time for the "Last modified" field that we are going to
 * transform to the correct format.
 * @param content_type As described in HTML RFC.
 * @param content_length Length of the content that will follow this header.
 * @return Buffer containing all the header including the CRLF separator that
 * will go before the body.
 */
char *header_craft(char *buf, int minor_version, STATUS_CODE status_code,
                   char *server_signature, time_t *last_modified, char *content_type,
                   int content_length, int options, SCRIPT_TYPE script);

#endif //RESPONSE_HEADER_H
