title process_post

participant response

activate response

response->response: Get content type
activate response
response<<--response: type
deactivate response

response->response: Execute process
activate response

response-->response: file descriptor
deactivate response

response->response_header: Create header
activate response_header

response_header->utils: Transform time to specific format
activate utils
utils-->response_header: time
deactivate utils


response_header-->response: header
deactivate response_header


