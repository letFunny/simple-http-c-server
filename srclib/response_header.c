#include <stdio.h>
#include <time.h>

#include <response_header.h>
#include <utils.h>

const int STATUS_NUM[] = {
        FOR_EACH_CODE(GENERATE_NUM)
};
const char *STATUS_STRINGS[] = {
        FOR_EACH_CODE(GENERATE_STRING)
};

char *header_craft(char *buf, int minor_version, STATUS_CODE status_code,
                   char *server_signature, time_t *last_modified, char *content_type,
                   int content_length, int options, SCRIPT_TYPE script) {
    char date[100];
    char str_last_mod[100];
    time_t now;

    // Get the current time for that Date header
    if ((now = time(0)) == -1) {
        // System failed in obtaining the current time
        return NULL;
    }
    // Transform the times into the specified format.
    if (-1 == time_to_string(&now, date, 100))
        return NULL;

	// If the verb is OPTIONS or the status code is 405, we have to include
	// allow in the header.
    if (options || status_code == S_405) {
        if (script != NONE) {
            sprintf(buf, "HTTP/1.%d %d %s\r\n"
                         "Allow: POST, GET, OPTIONS\r\n"
                         "Date: %s\r\n"
                         "Server: %s\r\n"
                         "Content-Length: %d\r\n"
                         "\r\n",
                    minor_version, STATUS_NUM[status_code],
                    STATUS_STRINGS[status_code], date, server_signature,
                    content_length);
            return buf;
        } else {
            sprintf(buf, "HTTP/1.%d %d %s\r\n"
                         "Allow: GET, OPTIONS\r\n"
                         "Date: %s\r\n"
                         "Server: %s\r\n"
                         "Content-Length: %d\r\n"
                         "\r\n",
                    minor_version, STATUS_NUM[status_code],
                    STATUS_STRINGS[status_code], date, server_signature,
                    content_length);
            return buf;
        }
    }

    if (last_modified == NULL) {
        sprintf(buf, "HTTP/1.%d %d %s\r\n"
                     "Date: %s\r\n"
                     "Server: %s\r\n"
                     "Content-Length: %d\r\n"
                     "Content-Type: %s\r\n"
                     "\r\n",
                minor_version, STATUS_NUM[status_code],
                STATUS_STRINGS[status_code], date, server_signature,
                content_length, content_type);
        return buf;
    }

    if (-1 == time_to_string(last_modified, str_last_mod, 100))
        return NULL;
    sprintf(buf, "HTTP/1.%d %d %s\r\n"
                 "Date: %s\r\n"
                 "Server: %s\r\n"
                 "Last-Modified: %s\r\n"
                 "Content-Length: %d\r\n"
                 "Content-Type: %s\r\n"
                 "\r\n",
            minor_version, STATUS_NUM[status_code],
            STATUS_STRINGS[status_code], date, server_signature,
            str_last_mod, content_length, content_type);
    return buf;
}
