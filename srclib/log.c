#include <log.h>

void log_message(int priority, const char *format, ...) {
    va_list ap;

    va_start(ap, format);
    if (daemon_mode) {
        vsyslog(priority, format, ap);
    } else {
        vfprintf(stderr, format, ap);
    }
    va_end(ap);
}
