#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <netdb.h>
#include <poll.h>

#include <request_parse.h>
#include <response_header.h>
#include <response.h>
#include <log.h>
#include <socket.h>

int socket_ini(long int listen_port) {
    int sockfd, ret;
    int option = 1;
    struct addrinfo hints, *res;
    char s_listen_port[6];
    //transform long int to string
    sprintf(s_listen_port, "%ld", listen_port);

    //initialize socket attributes
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    //get Internet address
    ret = getaddrinfo(NULL, s_listen_port, &hints, &res);
    if (ret != 0) {
        log_message(LOG_ERR, "%s\n", gai_strerror(ret));
        exit(EXIT_FAILURE);
    }

    //Create TCP socket
    if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0) {
        perror("Error en la creación del socket\n");
        freeaddrinfo(res);
        return -1;
    }

    //allow reuse of local address if there is no active listening socket
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    //aasign res->ai_addr to the socket referred by sockfd
    if (-1 == bind(sockfd, res->ai_addr, res->ai_addrlen)) {
        perror("bind");
        freeaddrinfo(res);
        exit(EXIT_FAILURE);
    }

    //socket starts listening with a queue of 10 clients
    if (listen(sockfd, 10) != 0) {
        freeaddrinfo(res);
        perror("socket--listen");
        return -1;
    }

    freeaddrinfo(res);
    return sockfd;
}

void connection(int clientfd, struct sockaddr_in client_addr, config *conf) {
    ssize_t ssret;
    char buf[4096];
    int close_connection;
    struct pollfd client;
    int ret;

    log_message(LOG_INFO, "Connection from [%s:%d]\n",
                inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

    client.fd = clientfd;
    client.events = POLLIN;

    while (1) {
        //If the client doesn't send anything in TIME_OUT_SEC, the connection is automatically closed
        ret = poll(&client, 1, TIME_OUT_SEC * 1000);
        //-1 if there was an error, 0 if there was a timeout, either way we close the connection
        if (ret == -1) {
            log_message(LOG_ERR, "Error in poll: %m\n");
            break;
        }
        if (ret == 0) {
            log_message(LOG_DEBUG, "Timeout in connection\n");
            break;
        }

        ssret = recv(clientfd, buf, 4096 * sizeof(char), 0);
        // If there's an error we close the connection
        if (ssret == -1) {
            log_message(LOG_ERR, "Error in recv: %m\n");
            break;
        }
        // The client has closed the connection
        if (ssret == 0) {
            log_message(LOG_DEBUG, "Client has closed the connection\n");
            break;
        }

        // Very importat line as recv does not put the last literal 0 to the string.
        buf[ssret] = 0;

        //If the server couldn't send the message we close the connection.
        if (process_response(buf, ssret, clientfd, conf, &close_connection) == SEND_ERROR) {
            log_message(LOG_ERR, "Error sending message\n");
            break;
        }

        //If the client specifies Connection: close we close the connecion
        if (close_connection == 1) {
            log_message(LOG_DEBUG, "Client specified Connection: close\n");
            break;
        }
        //log_message(LOG_DEBUG,"\nRespuesta procesada");fflush(stdout);
    }
    log_message(LOG_INFO, "Close connection [%s:%d]\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
    close(clientfd);
}
