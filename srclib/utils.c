#include <utils.h>

int time_to_string(const time_t *timep, char *buf, size_t length) {
    struct tm *tm;

    if ((tm = gmtime(timep)) == NULL) {
        // Error passing the time_t object to a detailed structure
        return -1;
    }
    if (0 == strftime(buf, length, "%a, %d %b %Y %H:%M:%S %Z", tm)) {
        // Error passing the structure to string in the detailed format
        return -1;
    }
    return 0;
}

char *get_file_extension(const char *path) {
    char *p;

    p = strrchr(path, '.');
    return p == NULL ? p : p + 1;
}
