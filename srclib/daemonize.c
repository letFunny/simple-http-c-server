#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>

#include <daemonize.h>

#define EXIT_FAIL(cond, message) if (cond) {perror(message);exit(EXIT_FAILURE);}

void daemonize(void) {
    int fd_null;
    pid_t my_pid;

    EXIT_FAIL((my_pid = fork()) < 0, "Error in fork");

    if (my_pid > 0)
        exit(EXIT_SUCCESS);

    EXIT_FAIL(setsid() < 0, "Error in setsid");
    EXIT_FAIL((my_pid = fork()) < 0, "Error in fork");

    if (my_pid > 0)
        exit(EXIT_SUCCESS);

    umask(0);

    chdir("/");

    fprintf(stderr, "PID of the parent is %d\n", getpid());

    fd_null = open("/dev/null", O_RDWR);
    dup2(fd_null, STDIN_FILENO);
    dup2(fd_null, STDOUT_FILENO);
    dup2(fd_null, STDERR_FILENO);

    //open log
    openlog(NULL, LOG_PID, LOG_DAEMON);
    syslog(LOG_NOTICE, "Daemon started");
    closelog();
}
