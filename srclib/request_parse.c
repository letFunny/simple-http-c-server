#define _XOPEN_SOURCE 700

#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <response_header.h>

#include <request_parse.h>
#include <log.h>
#include <utils.h>

void print_request(request *req) {
    log_message(LOG_DEBUG, "Method: %s\n", req->method);
    log_message(LOG_DEBUG, "Path: %s\n", req->path);
    log_message(LOG_DEBUG, "Minor version: %d\n", req->minor_version);
    log_message(LOG_DEBUG, "Num Headers: %zu\n", req->num_headers);
    if (req->num_headers > 0) {
        log_message(LOG_DEBUG, "Headers:\n");
        for (size_t i = 0; i != req->num_headers; ++i) {
            log_message(LOG_DEBUG, "%s: %s\n", req->headers[i].name, req->headers[i].value);
        }
    }
    if (req->parameters) {
        log_message(LOG_DEBUG, "Get parameters:\n");
        log_message(LOG_DEBUG, "%s\n", req->parameters);
        /* for (i = 0; req->parameters[i]; i++) {
            log_message(LOG_DEBUG,"%s\n", req->parameters[i]);
        }*/
    }
    if (req->body) {
        log_message(LOG_DEBUG, "BODY:\n");
        log_message(LOG_DEBUG, "%s\n", req->body);
    }
}


/* This function is meant to be called to compare times for the
 * If-Modified-Since header. It returns 0 if t1 <= t2, 1 if t1 > t2 and -1
 * on error.
 */
static int compare_time(const time_t *t1, const char *t2) {
    struct tm tm = {0};
    time_t t2_time;
    double diff;

    if (NULL == strptime(t2, "%a, %d %b %Y %H:%M:%S %Z", &tm)) {
        // If the funcion fails we assume it is a parsing error and return it.
        return -1;
    }

    if ((time_t) -1 == (t2_time = mktime(&tm))) {
        // If the funcion fails we assume it is a parsing error and return it.
        return -1;
    }
    diff = difftime(*t1, t2_time);

    if (diff > 0.0)
        return 1;
    else
        return 0;
}

static request_error is_request_valid(request *req) {
    struct stat s;
    int not_modified = 0;

    if (strncmp(req->method, "GET", 3 + 1) != 0 &&
        strncmp(req->method, "POST", 4 + 1) != 0 &&
        strncmp(req->method, "OPTIONS", 7 + 1) != 0) {
        return METHOD_NOT_ALLOWED;
    }

    if (1 != req->minor_version)
        return INVALID_VERSION;

    if (strncmp(req->method, "OPTIONS", 8) == 0 &&
        req->path[strlen(req->path) - 1] == '*') {
        return OK;
    }

    // If GET or POST
    // We open the file and fill the fields only if we receive a GET request
    // with no intention to execute a script.
    if (-1 == (req->fd = open(req->path, O_RDONLY)))
        return INVALID_FILE;

    if (-1 == fstat(req->fd, &s))
        return INVALID_FILE;

    req->last_modified = s.st_mtime;
    req->file_size = s.st_size;
    // Check if it is regular file
    if (!S_ISREG(s.st_mode))
        return INVALID_FILE;

    req->extension = get_file_extension(req->path);
    if (strncmp(req->extension, "py", 3) == 0) {
        req->script = PYTHON;
    } else if (strncmp(req->extension, "php", 4) == 0) {
        req->script = PHP;
    } else {
        req->script = NONE;
    }

	// You cannot post to a regular file, only to scripts
	if (req->script == NONE && strncmp(req->method, "POST", 4 + 1) == 0) {
		return METHOD_NOT_ALLOWED;
	}

    // We check header options
    if (req->num_headers > 0) {
        for (size_t i = 0; i != req->num_headers; ++i) {
            // We search for the If-Modified header
            if (strncmp(req->headers[i].name, "If-Modified-Since", 18) == 0) {
                int comp = compare_time(&(req->last_modified),
                                        req->headers[i].value);
                // If the request contains a time after the file last
                // modification date we return NOT_MODIFIED
                if (comp == 0)
                    not_modified = 1;
                else if (comp == -1)
                    // If there was an error, the time in the request was bad
                    // formatted.
                    return PARSER_ERROR;
            }
            // We search for the Connection: close
            if (strncmp(req->headers[i].name, "Connection", 11) == 0) {
                if (strncmp(req->headers[i].value, "close", 6) == 0)
                    req->close = 1;
            }
        }
    }


    if (not_modified == 1) return NOT_MODIFIED;

    // if (0 != access(req->path, R_OK)) {
    //         // If it is not GET we just need to check that the file exists
    //     return INVALID_FILE;
    // }

    return OK;
}

request_error request_parse(char *buf, int buflen, request *req, char *path) {
    int ret;
    char *pos;
    char *path_request;
    size_t method_len, path_len, i;
    size_t num_headers_max;

    num_headers_max = NUM_HEADERS;
    ret = phr_parse_request(buf, buflen, &(req->method), &method_len, (const char **) &path_request, &path_len,
                            &(req->minor_version), req->headers, &(num_headers_max), 0);
    req->num_headers = num_headers_max;
    req->parameters = NULL;
    req->body = NULL;
    req->last_modified = (time_t) -1;
    req->file_size = 0;
    req->fd = -1;
    // By default we dont close the connection
    req->close = 0;
    // The library does not null-termite the strings but just set the pointers inside the request buffer. We can set
    // the null bytes manually because each field is separated by at least a white space character.
    ((char *) req->method)[method_len] = 0;
    ((char *) path_request)[path_len] = 0;
    for (i = 0; i < req->num_headers; i++) {
        ((char *) (req->headers[i].name))[req->headers[i].name_len] = 0;
        ((char *) (req->headers[i].value))[req->headers[i].value_len] = 0;
    }

    // If the request is POST, it adds the body to the object
    if (strncmp(req->method, "POST", 5) == 0) {
        // The request body is ret characters after the beiginning. We can use
        // buf because the phr_parse works on the same memory it was given.
        req->body = buf + ret;
    }

    // Adds the config folder to the path
    strcpy(req->path, path);
    strcat(req->path, path_request);

    // Parse the get parameters
    if ((pos = strchr(req->path, '?'))) {
        *pos = 0;
        // req->parameters = get_parameters(pos + 1);
        req->parameters = pos + 1;
    }

    if (ret > 0)
        return is_request_valid(req);
    else if (ret == -2) {
        // If ret is -2, the request is incomplete
        return INCOMPLETE_REQUEST;
    } else {
        // If the request if -1, there has been a parse error and the request is not good formatted. There should
        // not be any other possible return value so we return PARSE_ERROR.
        return PARSER_ERROR;
    }
}
